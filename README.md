# README #

### What is this repository for? ###

* Common resources for the Deeper Pop CS 221 Project
* Version 1.0

##segment.py##
USAGE: python segment.py FILENAME RECEPTIVEFIELD
FILENAME should specify which audio file to cut from
RECEPTIVEFIELD can be any int, probably 512
A .wav file named segment.wav will be produced in the same directory