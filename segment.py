import wave, sys

if len(sys.argv) < 3:
	print 'Invalid arguments'
	sys.exit()
filename = sys.argv[1]
receptiveField = int(sys.argv[2])
win= wave.open(filename, 'rb')
wout= wave.open('segment.wav', 'wb')

t0, t1= 0.0, receptiveField/16000.0 # cut audio between one and two seconds
s0, s1= int(t0*win.getframerate()), int(t1*win.getframerate())

wout.setparams(win.getparams())
wout.writeframes(win.readframes(s1-s0))

win.close()
wout.close()
print 'Finished, file saved as segment.wav'